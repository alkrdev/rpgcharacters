﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Exceptions
{
    internal class InvalidWeaponException : Exception
    {
        public InvalidWeaponException(string message)
        {
            Console.WriteLine(message);
        }
    }
}
