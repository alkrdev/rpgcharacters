﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Exceptions
{
    internal class InvalidArmorException : Exception
    {
        public InvalidArmorException(string message)
        {
            Console.WriteLine(message);
        }
    }
}
