﻿using RPGCharacters.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Structs
{
    public struct AllowedEquipment
    {
        public List<WeaponType> Weapons;
        public List<ArmorType> Armors;
    }
}
