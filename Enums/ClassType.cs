﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Enums
{
    public enum ClassType
    {
        MAGE,
        RANGER,
        ROGUE,
        WARRIOR
    }
}
