﻿using RPGCharacters.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Models
{
    public class Equipment
    {
        public Weapon Weapon { get; set; }
        public Dictionary<SlotType, Armor> Worn { get; set; }
    }
}
