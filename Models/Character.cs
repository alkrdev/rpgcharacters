﻿using RPGCharacters.Enums;
using RPGCharacters.Exceptions;
using RPGCharacters.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Models
{
    public class Character
    {
        public event Action<ClassType> CharacterLeveledUp;

        private AllowedEquipment AllowedEquipment;

        private int level = 1;
        public ClassType Type { get; set; }
        public string Name { get; set; }
        public Attributes Attributes { get; set; }
        public Equipment Equipment { get; set; }
        public int Level
        {
            get { return level; }
        }

        public Character(ClassType type, string name)
        {
            Type = type;
            Name = name;
            Attributes = new Attributes(Type);
            CharacterLeveledUp += new Action<ClassType>(HandleLevelUpEvent);
            AllowedEquipment.Weapons = GetAllowedWeaponTypes(Type);
            AllowedEquipment.Armors = GetAllowedArmorTypes(Type);
        }

        public void LevelUp ()
        {
            level++;
            CharacterLeveledUp(Type);
        }
        private void HandleLevelUpEvent(ClassType type)
        {
            switch (type)
            {
                case ClassType.MAGE:
                    IncreaseAttributes(1, 1, 5);
                    break;
                case ClassType.RANGER:
                    IncreaseAttributes(1, 5, 1);
                    break;
                case ClassType.ROGUE:
                    IncreaseAttributes(1, 4, 1);
                    break;
                case ClassType.WARRIOR:
                    IncreaseAttributes(3, 2, 1);
                    break;
                default:
                    break;
            }
        }

        private void IncreaseAttributes(int str, int dex, int intel)
        {
            Attributes.Strength += str;
            Attributes.Dexterity += dex;
            Attributes.Intelligence += intel;
        }

        public void EquipWeapon(Weapon weap)
        {
            if (!AllowedEquipment.Weapons.Contains(weap.Type)) throw new InvalidWeaponException($"A {Type} cannot equip a {weap.Type}");

            Equipment.Weapon = weap;
        }

        public void EquipArmor(Armor arm, SlotType slot)
        {
            if (!AllowedEquipment.Armors.Contains(arm.Type)) throw new InvalidArmorException($"A {Type} cannot equip a {arm.Type} type armor");

            Equipment.Worn[slot] = arm;
        }

        private List<WeaponType> GetAllowedWeaponTypes(ClassType type) => type switch
        {
            ClassType.MAGE => new List<WeaponType>() { WeaponType.STAFF, WeaponType.WAND },
            ClassType.RANGER => new List<WeaponType>() { WeaponType.BOW },
            ClassType.WARRIOR => new List<WeaponType>() { WeaponType.AXE, WeaponType.HAMMER, WeaponType.SWORD },
            ClassType.ROGUE => new List<WeaponType>() { WeaponType.DAGGER, WeaponType.SWORD },
            _ => throw new NotImplementedException()
        };

        private List<ArmorType> GetAllowedArmorTypes(ClassType type) => type switch
        {
            ClassType.MAGE => new List<ArmorType>() { ArmorType.CLOTH },
            ClassType.RANGER => new List<ArmorType>() { ArmorType.LEATHER, ArmorType.MAIL },
            ClassType.WARRIOR => new List<ArmorType>() { ArmorType.MAIL, ArmorType.PLATE },
            ClassType.ROGUE => new List<ArmorType>() { ArmorType.LEATHER, ArmorType.MAIL },
            _ => throw new NotImplementedException()
        };
    }
}
