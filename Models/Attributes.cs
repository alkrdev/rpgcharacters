﻿using RPGCharacters.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Models
{
    public class Attributes
    {
        private int strength;
        private int dexterity;
        private int intelligence;
        public int Strength
        {
            get { return strength; }
            set { strength = value; }
        }
        public int Dexterity
        {
            get { return dexterity; }
            set { dexterity = value; }
        }
        public int Intelligence
        {
            get { return intelligence; }
            set { intelligence = value; }
        }
        public Attributes(ClassType type)
        {
            switch (type)
            {
                case ClassType.MAGE:
                    Strength = 1;
                    Dexterity = 1;
                    Intelligence = 8;
                    break;
                case ClassType.RANGER:
                    Strength = 1;
                    Dexterity = 7;
                    Intelligence = 1;
                    break;
                case ClassType.ROGUE:
                    Strength = 2;
                    Dexterity = 6;
                    Intelligence = 1;
                    break;
                case ClassType.WARRIOR:
                    Strength = 5;
                    Dexterity = 2;
                    Intelligence = 1;
                    break;
                default:
                    break;
            }
        }
    }
}
