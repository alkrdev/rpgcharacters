﻿using RPGCharacters.Models;
using RPGCharacters.Enums;
using RPGCharacters.Exceptions;

namespace Surface
{
    public class Program
    {
        public static void Main()
        {
            Character character = new Character(ClassType.MAGE, "Casper");

            Console.WriteLine("Name: " + character.Name);

            Console.WriteLine("Strength: " + character.Attributes.Strength);
            Console.WriteLine("Dexterity: " + character.Attributes.Dexterity);
            Console.WriteLine("Intelligence: " + character.Attributes.Intelligence);
            Console.WriteLine("Level: " + character.Level);

            //Weapon weap = new Weapon();
            //weap.Type = WeaponType.BOW;

            //Armor arm = new Armor();
            //arm.Type = ArmorType.MAIL;

            //try
            //{
            //    character.EquipArmor(arm, SlotType.BODY);
            //}
            //catch (Exception ex)
            //{
            //    if (ex is InvalidWeaponException || ex is InvalidArmorException)
            //    {

            //    }
            //}


            //character.LevelUp();
            //Console.WriteLine("Name: " + character.Name);

            //Console.WriteLine("Strength: " + character.Attributes.Strength);
            //Console.WriteLine("Dexterity: " + character.Attributes.Dexterity);
            //Console.WriteLine("Intelligence: " + character.Attributes.Intelligence);
            //Console.WriteLine("Level: " + character.Level);

            Console.ReadKey();
        }
    }
}


//Console.WriteLine("Level: " + character.Level);